$(function(){
    $('.search-activate').on('click',function(){
        if($(this).hasClass('fa-search')){
            $(this).removeClass('fa-search').addClass('fa-times');
            $('.search').removeClass('inactive').addClass('active');
        }else{
            $(this).removeClass('fa-times').addClass('fa-search');
            $('.search').removeClass('active').addClass('inactive');
        }
    });

    $('.open-side-menu').on('click',function(){
        $('.side-menu').removeClass('inactive').addClass('active')
    });
    $('.close-side-menu').on('click',function(){
        $('.side-menu').removeClass('active').addClass('inactive')
    });
    $('.mobile-chapter-navigation li').on('click', function(){
        $('.paragraph').removeClass('active');
        $($(this).data('id')).addClass('active');
        $('.mobile-chapter-navigation li').removeClass('prev').removeClass('next');
        if($(this).is(':last-child')){
            $(this).parent().find('li:first-child').addClass('next');
            $(this).prev().addClass('prev');
        }else if($(this).is(':first-child')){
            $(this).next().addClass('next');
            $(this).parent().find('li:last-child').addClass('prev');
        }else{
            $(this).next().addClass('next');
            $(this).prev().addClass('prev');
        }
    });
    $.smartbanner({
        title: 'CustomsMobile',
        author: 'CustomsMobile'
    });
});